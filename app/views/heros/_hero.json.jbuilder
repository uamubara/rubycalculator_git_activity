json.extract! hero, :id, :first_name, :last_name, :alias, :address, :city, :state, :car_id, :created_at, :updated_at
json.url hero_url(hero, format: :json)
