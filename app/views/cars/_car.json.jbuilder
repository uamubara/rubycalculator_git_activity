json.extract! car, :id, :name, :make, :model, :year, :color, :created_at, :updated_at
json.url car_url(car, format: :json)
