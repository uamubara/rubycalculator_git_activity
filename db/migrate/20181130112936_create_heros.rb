class CreateHeros < ActiveRecord::Migration[5.2]
  def change
    create_table :heros do |t|
      t.string :first_name
      t.string :last_name
      t.string :alias
      t.string :address
      t.string :city
      t.string :state
      t.references :car, foreign_key: true

      t.timestamps
    end
  end
end
